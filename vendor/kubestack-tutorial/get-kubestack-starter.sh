#!/usr/bin/env bash

# From: https://www.kubestack.com/framework/documentation/tutorial-build-local-lab 

# Download and unpack the KinD starter. Remove the archive file.
# curl -LO https://storage.googleapis.com/quickstart.kubestack.com/kubestack-starter-kind-v0.9.0-beta.0.zip
wget https://storage.googleapis.com/quickstart.kubestack.com/kubestack-starter-kind-v0.9.0-beta.0.zip 
unzip kubestack-starter-kind-v0.9.0-beta.0.zip -d $HOME/kubestack-starter
rm kubestack-starter-kind-v0.9.0-beta.0.zip
# Change into the KinD starter directory and initialize your Git repo.
cd kubestack-starter-kind
git init .
git add .
git commit -m "Initialized from KinD starter"

