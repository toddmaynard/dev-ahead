#!/usr/bin/env bash

wget https://releases.hashicorp.com/terraform/0.12.26/terraform_0.12.26_linux_amd64.zip
unzip terraform_0.12.26_linux_amd64.zip
sudo mv terraform_0.12.26_linux_amd64/terraform /usr/local/bin