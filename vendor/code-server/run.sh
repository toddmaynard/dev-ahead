#!/usr/bin/env bash
#
# From: https://github.com/cdr/code-server/blob/v3.5.0/doc/install.md#docker
#
# This will start a code-server container and expose it at http://127.0.0.1:8080.
# It will also mount your current directory into the container as `/home/coder/project`
# and forward your UID/GID so that all file system operations occur as your user outside
# the container.
#
# Your $HOME/.config is mounted at $HOME/.config within the container to ensure you can
# easily access/modify your code-server config in $HOME/.config/code-server/config.json
# outside the container.

docker run -it -p 127.0.0.1:8080:8080 \
	-v "$HOME/Projects/toddmaynard/dev-ahead/vendor/code-server/.config:/home/coder/.config" \
	-v "$HOME/Projects/toddmaynard/dev-ahead:/home/coder/project" \
	-v "/var/run/docker.sock:/var/run/docker.sock" \
	-u "$(id -u):$(id -g)" \
	codercom/code-server:latest
